package com.test.user.testapp;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

public class WebServer extends NanoHTTPD {

    public final static int PORT = 2222;

    public WebServer(int port) {
        super(port);
    }

    @Override
    public Response serve(IHTTPSession session) {

        Map<String, String> parms = session.getParms();
        Map<String, String> files = new HashMap<>();

        try {
            session.parseBody(files);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ResponseException e) {
            e.printStackTrace();
        }
        String uri = session.getUri();
        File rootDir = Environment.getExternalStorageDirectory();
        File[] filesList;
        String filepath = "";
        if (uri.trim().isEmpty()) {
            filesList = rootDir.listFiles();
        } else {
            if (uri.trim().equals("/")) {
                filepath = "/sdcard";
            } else {
                filepath = filepath + uri.trim();
            }
            filesList = new File(filepath).listFiles();
        }

        StringBuilder answer = new StringBuilder();
        answer.append("<html>")
                .append("<head>")
                .append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">")
                .append("<h1>").append(uri).append("</h1> ")
                .append("</head> ")
                .append("<body>");

        File newFile = new File(filepath);
        if (newFile.isDirectory()) {
            for (File detailsOfFiles : filesList) {
                answer.append("<a href=\"")
                        .append(detailsOfFiles.getAbsolutePath())
                        .append("\" alt = \"\">")
                        .append(detailsOfFiles.getName())
                        .append("</a><br>");
            }
        } else {
            return downloadFile(newFile);
        }

        answer.append("</body></html>");
        return newFixedLengthResponse(answer.toString());
    }

    private Response downloadFile(File file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            Log.e("downloadFile", ex.getMessage());
        }
        return newFixedLengthResponse(Response.Status.OK, "application/octet-stream", fis, file.length());
    }
}