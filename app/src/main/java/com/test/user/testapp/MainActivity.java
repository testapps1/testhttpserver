package com.test.user.testapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final static int PERMISSION_REQUEST = 2;
    private boolean isStarted = false;

    private TextView textViewIpAccess;
    private TextView textUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST);
            }
        }
        textViewIpAccess = findViewById(R.id.textViewIpAccess);
        textUri = findViewById(R.id.textUri);

        final Button button = findViewById(R.id.turnButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isStarted) {
                    isStarted = true;
                    startService(new Intent(getApplicationContext(), HttpServerService.class));
                    textViewIpAccess.setText(getIpAccess());
                    button.setText(getString(R.string.turn_off));
                    textUri.setText(getString(R.string.phone_uri));
                    textViewIpAccess.setVisibility(View.VISIBLE);
                } else {
                    isStarted = false;
                    button.setText(getString(R.string.turn_on));
                    stopService(new Intent(getApplicationContext(), HttpServerService.class));
                    textUri.setText(getString(R.string.server_is_turned_off));
                    textViewIpAccess.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("ACTIVITY", "PERMISSION_GRANTED");
                } else {
                    Log.d("ACTIVITY", "HOLY CRAP");
                }
            }
        }
    }

    private String getIpAccess() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager != null ? wifiManager.getConnectionInfo().getIpAddress() : 0;
        final String formattedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        return "http://" + formattedIpAddress + ":" + WebServer.PORT;
    }
}
